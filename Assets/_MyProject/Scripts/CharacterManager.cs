﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterManager : MonoBehaviour {
	
	//public PlayerProfile playerProfile;
	public GameControl gameControl;
	public GameObject enemy;
	
	private CharacterImageController imageController;
	private EnemyManager enemyManager;	
	private Integers integers;

	public Text damageText;
	public Text levelText;
	public Text experienceText;
	public GameObject critText1;
	public GameObject critText2;

	public int defaultRequiredExp = 100;
	public int defaultCharacterExp = 0;
	public int defaultLevel = 1;
	public float critRate = 0.2f;
	public int critDmg = 10;
	public int powerDmg = 3;

	private int currentLevel;
	private int[] currentDamages;
	private int[] currentRequiredExps;
	private int[] currentCharacterExps;
	private int attackCounter = 0;


	private void Awake ()
	{
		this.imageController = GetComponent<CharacterImageController> ();
		this.enemyManager = enemy.GetComponent<EnemyManager> ();
		this.integers = new Integers ();
		
		this.currentLevel = defaultLevel;
		this.currentRequiredExps = integers.IntegerToArray (defaultRequiredExp);
		this.currentCharacterExps = integers.IntegerToArray (defaultCharacterExp);
		this.currentDamages = integers.IntegerToArray (currentLevel);

		levelText.text = "Lvl = " + currentLevel;
		experienceText.text = "Exp = " + integers.ArrayToString (currentCharacterExps) + "/" + integers.ArrayToString (currentRequiredExps);
		damageText.text = "Dmg = " + integers.ArrayToString (currentDamages);
	}

	private void Update ()
	{

	}
	
	private void DisappearCritText()
	{
		critText1.SetActive(false);
		critText2.SetActive(false);
	}

	//attackType = 1 : normalAttack, 2 : PowerAttack
	public void HitEnemy(int attackType)
	{				
		if (attackCounter >= 50)
		{
			attackCounter = 0;
			enemyManager.SummonBoss();
		}

		float rate = Random.Range(0f,1f);

		switch (attackType)
		{
		case 1:
			imageController.AnimateNormalAttack();
			attackCounter += 1;
			if (rate >= 1f - critRate)
			{
				enemyManager.Hitted(integers.MultiplyArray(currentDamages, critDmg));
				critText1.SetActive(true);
				Invoke ("DisappearCritText", 1f);
			}
			else
			{
				enemyManager.Hitted(currentDamages);
			}
			break;
		case 2:
			imageController.AnimatePowerAttack();
			
			if (rate >= 1f - critRate)
			{
				enemyManager.Hitted(integers.MultiplyArray(currentDamages, critDmg * powerDmg));
				critText2.SetActive(true);
				Invoke ("DisappearCritText", 1f);
			}
			else
			{
				enemyManager.Hitted(integers.MultiplyArray(currentDamages, powerDmg));
			}
			break;
		default:
			return;
		}
	}

	public void GainExp(int[] exps)
	{
		currentCharacterExps = integers.AdditionArray(currentCharacterExps, exps);

		int counter = 0;
		if (integers.CompareArray (currentCharacterExps, currentRequiredExps))
		{
			while (integers.CompareArray(currentCharacterExps, currentRequiredExps))
			{
				currentCharacterExps = integers.SubtractionArray (currentCharacterExps, currentRequiredExps);
				counter += 1;
				LevelUp ();
			}
			Debug.Log ("LevelUp : " + counter);
		}
		UpdateUI ();
	}
	
	private void LevelUp()
	{
		currentLevel += 1;
						
		UpdateUI ();
	}

	private void CalculateByLevel()
	{
		this.currentRequiredExps = integers.MultiplyArray(integers.IntegerToArray(currentLevel), (currentLevel * 10));		
		this.currentDamages = integers.MultiplyArray(integers.IntegerToArray(currentLevel), (currentLevel) * (1 + (currentLevel / 100)));
	}

	public void SetCurrentLevel(int level)
	{
		this.currentLevel= level;
	}

	public int GetCurrentLevel()
	{
		return this.currentLevel;
	}
	
	public int[] GetDamage()
	{
		return currentDamages;
	}

	public void UpdateUI()
	{
		CalculateByLevel ();
		levelText.text = "Lvl = " + currentLevel;
		experienceText.text = "Exp = " + integers.ArrayToString (currentCharacterExps) + "/" + integers.ArrayToString (currentRequiredExps);
		damageText.text = "Dmg = " + integers.ArrayToString (currentDamages);
	}
}
