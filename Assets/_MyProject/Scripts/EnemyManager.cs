﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyManager : MonoBehaviour {
	
	public GameControl gameControl;
	public GameObject character;
	public GameObject image1;
	public GameObject image2;
	public GameObject image3;
	public GameObject winText;
	public GameObject nameText;
	public GameObject bgm;
	public Text hitPointText;

	private CharacterManager characterManager;
	private BGMController bgmController;
	private Integers integers;

	public int expPerDmg = 1;
	public int defaultEnemyLevel = 1;
	public int defaultFullHitPoint = 10;

	private int[] defaultFullHitPoints;
	private int[] currentHitPoints;
	private int[] fullHitPoints;
	private int currentEnemyLevel;

	private bool halfHitPoint;
	private bool deadlyHitPoint;
	private bool dead;
	private bool summonBoss;

	
	private void Awake()
	{
		this.characterManager = character.GetComponent<CharacterManager> ();
		this.bgmController = bgm.GetComponent<BGMController> ();
		this.integers = new Integers ();

		this.currentHitPoints = new int[integers.integersSize];
		this.defaultFullHitPoints = integers.IntegerToArray(defaultFullHitPoint);
		this.currentEnemyLevel = defaultEnemyLevel;

		Init ();
	}

	public void Init()
	{
		//this.currentEnemyLevel = gameControl.LoadEnemyData ();

		CaculateByLevel ();

		this.currentHitPoints = fullHitPoints;
		this.dead = false;
		this.deadlyHitPoint = false;
		this.halfHitPoint = false;
		this.summonBoss = false;
		this.nameText.SetActive (true);
		winText.SetActive(false);

		UpdateUI ();

		if (currentEnemyLevel >= 5)
		{
			image1.SetActive (false);
			image3.SetActive (false);
			image2.SetActive (true);
			bgmController.SetActiveAudio (2);
		}
		else
		{
			image3.SetActive (false);
			image2.SetActive (false);
			image1.SetActive (true);
			bgmController.SetActiveAudio(1);
		}
		
		UpdateUI ();
	}

	public void UpdateUI()
	{				
		if (integers.CompareArray(integers.DivisionArray(fullHitPoints, 2), currentHitPoints) && halfHitPoint == false)
		{
			this.halfHitPoint = true;
			hitPointText.color = Color.yellow;
		}
		if (integers.CompareArray(integers.DivisionArray(fullHitPoints, 5), currentHitPoints) && deadlyHitPoint == false)
		{
			this.deadlyHitPoint = true;
			hitPointText.color = Color.red;
		}
		if (integers.CheckZeroArray(currentHitPoints) && dead == false)
		{
			this.dead = true;
			this.currentEnemyLevel += 1;
			hitPointText.color = Color.black;
			winText.SetActive(true);
			gameControl.SaveEnemyData(currentEnemyLevel);

			Invoke ("Init", 5f);
		}
		
		hitPointText.text = integers.ArrayToString(currentHitPoints) + "/" + integers.ArrayToString(fullHitPoints);
	}
	
	public void Hitted (int[] damages)
	{
		if (dead != true)
		{
			this.currentHitPoints = integers.SubtractionArray(currentHitPoints, damages);
			characterManager.GainExp (integers.MultiplyArray(damages, expPerDmg));
			UpdateUI();
		}
	}

	public void SummonBoss ()
	{
		if (!summonBoss)
		{
			this.summonBoss = true;
			this.currentHitPoints = fullHitPoints;
			image1.SetActive (false);
			image2.SetActive (false);
			image3.SetActive (true);
			nameText.SetActive (false);
			bgmController.SetActiveAudio(3);

			UpdateUI();
		}
	}	
	
	private void CaculateByLevel()
	{		
		int[] results = defaultFullHitPoints;
		for (int i = 1; i <= 1 + (currentEnemyLevel / 2); i++)
		{
			results = integers.MultiplyArray (results, currentEnemyLevel * 10);
		}
		this.fullHitPoints = results;
	}

	public void SetEnemeyLevel(int level)
	{
		this.currentEnemyLevel = level;
	}

	private void Ending()
	{
		winText.GetComponent<Text> ().text = "Thank you for playing!!\n" + "Game Over";
	}
}
