﻿using UnityEngine;
using System.Collections;

public class CharacterImageController : MonoBehaviour {

	private Animator anim;


	private void Awake()
	{		
		anim = GetComponent<Animator> ();
	}

	public void AnimateNormalAttack ()
	{
		//Debug.Log ("triggerNormalAttack");
		anim.SetTrigger("NormalAttack");
	}

	public void AnimatePowerAttack ()
	{
		//Debug.Log ("triggerPowerAttack");
		anim.SetTrigger("PowerAttack");
	}
}
