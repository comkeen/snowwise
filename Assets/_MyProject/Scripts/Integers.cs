﻿using UnityEngine;
using System.Collections;

public class Integers
{
	public int numeralValue;
	public int integersSize;
	public int textSize;


	public Integers()
	{
		this.numeralValue = 10;
		this.integersSize = 64;
		this.textSize = 5;
	}

	public int[] IntegerToArray(int input)
	{
		int position = input.ToString().Length;
		int[] results = new int[integersSize];

		if (input < 0)
		{
			Debug.LogError("IntegerToArray(int input) parameter must be 양의 정수!!");
			return null;
		}

		for (int i = 0; i < position; i++)
		{
			results[i] = input % numeralValue;
			input = input / numeralValue;
			//Debug.Log ("input " + input + " TransformToArray index " + i + " value = " + results[i]);
		}
		return results;
	}

	public string ArrayToString(int[] integers)
	{
		string content = "";
		string position = "";

		int size = GetArrayLength (integers);
		int end = 0;

		if (size > textSize)
		{
			end = size - textSize;

			switch ((end + 2) / 3)
			{
			case 0:
				position = "";
				break;
			case 1:
				position = "k";
				break;
			case 2:
				position = "m";
				break;
			case 3:
				position = "b";
				break;
			case 4:
				position = "t";
				break;
			case 5:
				position = "quadril";
				break;
			case 6:
				position = "quintil";
				break;
			case 7:
				position = "sextil";
				break;
			case 8:
				position = "septil";
				break;
			case 9:
				position = "octil";
				break;
			case 10:
				position = "nonil";
				break;
			case 11:
				position = "decil";
				break;
			case 12:
				position = "undecil";
				break;
			case 13:
				position = "duodecil";
				break;
			case 14:
				position = "tredecil";
				break;
			case 15:
				position = "quattuordecil";
				break;
			case 16:
				position = "quindecil";
				break;
			case 17:
				position = "sexdecil";
				break;
			case 18:
				position = "septendecil";
				break;
			case 19:
				position = "octodecil";
				break;
			case 20:
				position = "novemdecil";
				break;
			case 21:
				position = "Vigintillion";
				break;

			default :
				position = "x10^" + (size - 1);
				break;
			}
		}

		for(int i = size - 1; i >= end; i--)
		{
			content += integers[i].ToString();


			if (i == end)
			{
				break;
			}
			else if((i % 3) == 0 && (i - end) < 3)
			{
				content += ".";
			}
			else if ((i % 3) == 0)
			{
				content += ",";
			}
		}
		content += position;
		return content;
	}
	
	public bool CompareArray(int[] lefts, int[] rights)
	{	
		if (GetArrayLength (lefts) > GetArrayLength (rights)) 
		{
			return true;
		}
		else if (GetArrayLength (lefts) == GetArrayLength (rights))
		{
			for(int i = (GetArrayLength (lefts)) - 1; i >= 0; i--)
			{
				if(lefts[i] < rights[i])
				{
					return false;
				}
				else if(lefts[i] > rights[i])
				{
					return true;
				}
				else if(lefts[i] == rights[i] && i == 0)
				{
					return true;
				}
			}
		}
		return false;
	}

	public int GetArrayLength(int[] inputs)
	{
		int counter = 1;
		for(int i = 0; i < integersSize-1; i++)
		{
			if (inputs[i] != 0)
			{
				counter = i + 1;
				//Debug.Log ("counter = " + counter);
			}
		}
		return counter;
	}
	
	public int[] AdditionArray(int[] lefts, int[] rights)
	{
		int result = 0;
		int over = 0;
		int[] results = new int[integersSize];
		
		for (int i = 0; i <= (GetArrayLength(lefts) + GetArrayLength(rights)) / 2; i++)
		{
			result = lefts[i] + rights[i] + over;
			
			results[i] = result % numeralValue;
			over = result / numeralValue;
		}
		
		return results;
	}
	
	public int[] SubtractionArray(int[] lefts, int[] rights)
	{
		int result = 0;
		int borrow = 0;
		
		int[] results = new int[integersSize];
		
		if (!CompareArray (lefts, rights))
		{
			SetZeroArray(results);
			return results;
		}
		
		for (int i = 0; i < integersSize; i++)
		{
			result = lefts[i] - rights[i] - borrow;
			if (result < 0)
			{
				results[i] = numeralValue + result;
				borrow = 1;
			}
			else
			{
				results[i] = result;
				borrow = 0;
			}
		}		
		return results;
	}

	public int[] MultiplyArray(int[] lefts, int right)
	{
		int result = 0;
		int over = 0;
		int[] results = new int[integersSize];
		
		for (int i = 0; i <= GetArrayLength(lefts) + right.ToString().Length; i++)
		{
			result = (lefts[i] * right) + over;

			results[i] = result % numeralValue;
			over = result / numeralValue;

			//Debug.Log ("MultiplyArray result " + i + " = " + results[i]);
		}
		
		return results;
	}

	public int[] DivisionArray(int[] lefts, int right)
	{
		int left = 0;
		int remain = 0;

		int[] results = new int[integersSize];
		
		for (int i = GetArrayLength(lefts) - 1; i >= 0; i--)
		{
			left = lefts[i] + (remain * numeralValue);
			
			results[i] = left / right;
			remain = left % right;
			
			//Debug.Log ("DivisionArray result " + i + " = " + results[i]);
		}
		
		return results;
	}

	public int MultiplySelf(int target, int time)
	{
		int result = 1;
		for(int i = 0; i<time; i++)
		{
			result = result * target;
		}
		return result;
	}

	public int[] SetZeroArray(int[] inputs)
	{
		for (int i = GetArrayLength(inputs) - 1; i >= 0; i--)
		{
			inputs[i] = 0;
		}
		return inputs;
	}

	public bool CheckZeroArray(int[] inputs)
	{
		if (GetArrayLength (inputs) == 1 && inputs [0] == 0) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
