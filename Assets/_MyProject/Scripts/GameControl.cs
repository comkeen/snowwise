﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization.Formatters.Soap;

public class GameControl : MonoBehaviour {

	public static GameControl gameControl;

	//public PlayerProfile playerProfile;
	public CharacterManager characterManager;
	public EnemyManager enemyManager;
	public BGMController bgmController;

	private void Awake ()
	{
		if (gameObject == null)
		{
			DontDestroyOnLoad (gameObject);
			gameControl = this;
		}

		else if (gameControl != this)
		{
			//Destroy (gameObject);
		}
	}

	public void StartLevel()
	{
		Debug.Log ("StartLevel");
		Application.LoadLevel (1);
	}

	public void OnApplicationQuit()
	{
		PlayerPrefs.Save();
	}

	public void QuitGame()
	{
		Debug.Log ("ExitGame");
		Application.Quit ();
	}

	public void Mute()
	{
		bgmController.SetAudioPlayMode ();
	}	
	
	public void DeleteData()
	{
		PlayerPrefs.DeleteAll();
	}
	
	public void SaveData()
	{
		PlayerPrefs.SetInt ("Level", characterManager.GetCurrentLevel());
		Debug.Log ("SaveData Level = " + characterManager.GetCurrentLevel());
	}
	
	public void LoadData()
	{
		characterManager.SetCurrentLevel (PlayerPrefs.GetInt ("Level"));
		Debug.Log ("LoadData Level = " + PlayerPrefs.GetInt ("Level"));
		characterManager.UpdateUI ();
	}

	public void ClearData()
	{
		PlayerPrefs.DeleteAll ();
	}

	public void SaveEnemyData(int level)
	{		
		PlayerPrefs.SetInt ("EenemyLevel", level);
		Debug.Log ("SaveEnemyData Level = " + level);
	}

	public int LoadEnemyData()
	{				
		Debug.Log ("LoadEnemyData Level = " + PlayerPrefs.GetInt ("EenemyLevel"));
		return PlayerPrefs.GetInt ("EenemyLevel");
	}

	public void OpenUrl()
	{
		Application.OpenURL("https://www.youtube.com/watch?v=F1qgBqiH4Og&list=PLhYr0ZCKafmDjllIka5OPg5RRklu2jxzJ&index=10");
	}

	/*
	public void Save()
	{
		BinaryFormatter binaryFormatter = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");
		
		_playerProfile = new PlayerProfile();
		Debug.Log (_playerProfile.damage);
		_playerProfile.damage = characterManager.getDamage();

		binaryFormatter.Serialize (file, _playerProfile);
		file.Close ();
	}
	
	public void Load()
	{
		if (File.Exists (Application.persistentDataPath + "/playerINfo.dat"))
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter ();
			FileStream file = File.OpenRead(Application.persistentDataPath + "/playerInfo.dat");
		
			_playerProfile = (PlayerProfile) binaryFormatter.Deserialize(file);
			_playerProfile.damage = characterManager.getDamage ();
			file.Close ();

			characterManager.setDamage((int)_playerProfile.damage);
		}
	}
	*/

	/*
	public void SaveProfile()
	{
		IFormatter formatter = new BinaryFormatter();
		//IFormatter formatter = new SoapFormatter();
		
		string fileName = "my.profile";
		
		FileStream stream = new FileStream( fileName, FileMode.Create );
		formatter.Serialize( stream, playerProfile );
		stream.Close();
	}
	
	public void LoadProfile()
	{
		IFormatter formatter = new BinaryFormatter();
		//IFormatter formatter = new SoapFormatter();
		
		string fileName = "my.profile";
		
		if( !File.Exists( fileName ) )
			return;
		
		FileStream stream = new FileStream( fileName, FileMode.Open );
		playerProfile = formatter.Deserialize( stream ) as PlayerProfile;
		stream.Close();
	}
	*/
}
