﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class BGMController : MonoBehaviour {

	public GameObject bgm; 

	public AudioMixerSnapshot playMusic1;
	public AudioMixerSnapshot playMusic2;
	public AudioMixerSnapshot playMusic3;
	public AudioClip[] musics;
	public AudioSource audioSource;
	public float bpm = 128f;

	private float m_Transition;
	private float m_QuarterNote;

	private bool playMusic;
	private int musicIndex; 


	private void Awake()
	{
		this.playMusic = true;				
		this.musicIndex = 1;

		m_QuarterNote = 60 / bpm;
		m_Transition = m_QuarterNote;
	}

	public void SetActiveAudio (int index)
	{
		switch (index)
		{
		case 1:
			this.musicIndex = 1;
			playMusic1.TransitionTo(m_Transition);
			break;
		case 2:
			this.musicIndex = 2;
			playMusic2.TransitionTo(m_Transition);
			break;
		case 3:
			this.musicIndex = 3;
			playMusic3.TransitionTo(m_Transition);
			break;
		default:
			return;
		}
	}

	public void SetAudioPlayMode ()
	{
		this.playMusic = !playMusic;
		bgm.SetActive (playMusic);

		if (playMusic)
		{
			SetActiveAudio(musicIndex);
		}
	}
}


