﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class OpenWebLink : MonoBehaviour, IPointerClickHandler {
	
	public string url;
	
	public void Start() {
		if (string.IsNullOrEmpty(url)) {
			Debug.LogWarning("No URL has been set in the OpenWebLink component, disabling component");
			enabled = false;
		}
	}
	
	public void OnPointerClick(PointerEventData eventData) {
		OpenURL();
	}
	
	public void OpenURL() {
		Application.OpenURL(url);
	}
	
	public void OpenURL(string aUrl) {
		Application.OpenURL(aUrl);
	}		
}