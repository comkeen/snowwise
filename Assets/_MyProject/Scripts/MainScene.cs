﻿using UnityEngine;
using System.Collections;

public class MainScene : MonoBehaviour {
	
	public GameObject loadingImage;
	public GameObject logoPanel;

	private Animator anim;
	private AudioSource boom;

	private int level;


	private void Start()
	{
		anim = GetComponent<Animator> ();
		boom = GetComponent<AudioSource> ();

		anim.SetTrigger("StartLogo");
		Invoke ("PlaySound", 1f);
	}

	public void LoadScene(int level)
	{
		Debug.Log ("LoadScene");
		this.level = level;
		loadingImage.SetActive(true);
		Invoke ("StartScene", 5f);
	}

	private void StartScene()
	{
		Application.LoadLevel(level);
	}

	public void ExitGame()
	{
		Debug.Log ("QuitGame");
		Application.Quit ();
	}

	private void PlaySound()
	{
		boom.Play ();
	}
}